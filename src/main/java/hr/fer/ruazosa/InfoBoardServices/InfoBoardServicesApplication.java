package hr.fer.ruazosa.InfoBoardServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoBoardServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoBoardServicesApplication.class, args);
	}

}
