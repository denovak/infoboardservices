package hr.fer.ruazosa.InfoBoardServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService{

    @Autowired
    private UserRepository repository;

    @Override
    public void createNewUser(User user) {
        repository.save(user);
    }

    @Override
    public List<User> findAll() {
        return (List<User>) repository.findAll();
    }

    @Override
    public boolean loginInUser(String username, String password) {
        if (repository.loginUser(username, password) != null) {
           return true;
        }
        return false;
    }

    @Override
    public boolean checkIfUsernameIsTaken(User user) {
        if (user == null) {
            throw new RuntimeException("User cannot be null");
        }

        if (user.getUserName() == null || user.getUserName().trim().equals("")) {
            throw new RuntimeException("User cannot be null or empty");
        }

        if (repository.findByUserName(user.getUserName()) != null) {
            return true;
        }

        return false;
    }


}
