package hr.fer.ruazosa.InfoBoardServices;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileInputStream;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    @Autowired
    IUserService userService;

    @GetMapping("/users")
    public List<User> findUsers() {

        return userService.findAll();
    }

    @PostMapping(path = "/users")
    public ResponseEntity<Object> registerUser(@RequestBody User user) {

        if (userService.checkIfUsernameIsTaken(user)) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", "Username already exists");
            return new ResponseEntity<Object>(body, HttpStatus.BAD_REQUEST);
        }
        userService.createNewUser(user);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @PostMapping(path = "/users/login")
    public ResponseEntity<Object> loginUser(@RequestBody User user) {
        if (!userService.loginInUser(user.getUserName(), user.getPassword())) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", "No such username or password");
            return new ResponseEntity<Object>(body, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Object>(HttpStatus.OK);
    }


    @GetMapping(path = "/users/sendpushmessage")
    public ResponseEntity<Object> sendPushMessage(@RequestParam(name="token") String token) {
        if (token.isEmpty()) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", "No token");
            return new ResponseEntity<Object>(body, HttpStatus.NOT_FOUND);
        }

        try {
            // TODO change this to point to place where service account data is stored
            FileInputStream serviceAccount =
                    new FileInputStream("/Users/dejannovak/Desktop/myfirebasedemoproject-79c20-firebase-adminsdk-lp9pw-d9260033d4.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();
            FirebaseApp.initializeApp(options);
        }
        catch (Exception exc) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", exc.getMessage());
            return new ResponseEntity<Object>(body, HttpStatus.BAD_REQUEST);
        }

        // This registration token comes from the client FCM SDKs
        Message message = Message.builder()
                .putData("title", "Some title")
                .putData("message", "Some message")
                .setNotification(new Notification("Some title", "Some message"))
                .setToken(token)
                .build();

        try {
            String response = FirebaseMessaging.getInstance().send(message);
            System.out.println("Successfully sent message: " + response);
        }
        catch (FirebaseMessagingException fme) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", fme.getMessage());
            return new ResponseEntity<Object>(body, HttpStatus.BAD_REQUEST);
        }



        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
